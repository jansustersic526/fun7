package api.v1.mappers;

import exceptions.Fun7Exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class Fun7ExceptionMapper implements ExceptionMapper<Fun7Exception> {

    @Override
    public Response toResponse(Fun7Exception e) {
        return Response.status(e.getErrorCode())
                .entity(e.getMessage())
                .build();
    }
}
