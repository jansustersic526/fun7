package api.v1.messages;

public class CheckServicesResponse {

    enum Status {
        enabled,
        disabled
    }

    private Status multiplayer;
    private Status userSupport;
    private Status ads;

    public Status getMultiplayer() {
        return multiplayer;
    }

    public void setMultiplayer(Status multiplayer) {
        this.multiplayer = multiplayer;
    }

    public void setMultipayer(boolean multipayer) {
        if (multipayer) {
            this.multiplayer = Status.enabled;
        } else {
            this.multiplayer = Status.disabled;
        }
    }

    public Status getUserSupport() {
        return userSupport;
    }

    public void setUserSupport(Status userSupport) {
        this.userSupport = userSupport;
    }
    public void setUserSupport(boolean userSupport) {
        if (userSupport) {
            this.userSupport = Status.enabled;
        } else {
            this.userSupport = Status.disabled;
        }
    }

    public Status getAds() {
        return ads;
    }

    public void setAds(Status ads) {
        this.ads = ads;
    }

    public void setAds(boolean ads) {
        if (ads) {
            this.ads = Status.enabled;
        } else {
            this.ads = Status.disabled;
        }
    }

    @Override
    public String toString() {
        return "multiplayer: " + multiplayer + "\nuserSupport: " + userSupport + "\nads: " + ads + "\n";
    }
}
