package api.v1.messages;

public class CheckServicesRequest {

    private String userId;
    private int timezone;
    private String countryCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "userId: " + userId + "\ntimezone: " + timezone + "\ncountryCode: " + countryCode + "\n";
    }
}
