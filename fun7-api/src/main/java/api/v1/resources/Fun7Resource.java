package api.v1.resources;

import api.v1.messages.CheckServicesRequest;
import api.v1.messages.CheckServicesResponse;
import beans.LogicBean;
import exceptions.Fun7Exception;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("fun7")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class Fun7Resource {

    @Inject
    private LogicBean logicBean;

    @POST
    @Path("checkServices")
    public Response checkServices(CheckServicesRequest input) throws Fun7Exception {

        CheckServicesResponse output = new CheckServicesResponse();

        output.setMultipayer(logicBean.checkMultiplayerStatus(input.getUserId(), input.getCountryCode()));
        output.setUserSupport(logicBean.checkCustomerSupportStatus());
        output.setAds(logicBean.checkAdsStatus(input.getCountryCode()));

        return Response.status(Response.Status.OK).entity(output).build();
    }

}
