import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class Fun7IT {

    /*
     * Firebase database contains 2 users:
     *      - test_user1 who used Fun7 6 times  (multiplayer should be enabled)
     *      - test_user2 who used Fun7 4 times  (multiplayer should be disabled)
     * */

    @ParameterizedTest
    @CsvSource({
            "test_user1, USA, enabled, 200",
            "test_user1, SLO, disabled, 200",
            "test_user2, USA, disabled, 200",
            "test_user2, SLO, disabled, 200",

    })
    public void verifyMultiplayerCheck(String userId, String countryCode, String expectedResult, int statusCode) {

        String requestBody = "{\n" +
                "    \"userId\": \"" + userId + "\",\n" +
                "    \"timezone\": 1,\n" +
                "    \"countryCode\": \"" + countryCode + "\"\n" +
                "}";
        RestAssured.baseURI  = "http://localhost:8080/v1/fun7/checkServices";

        given().contentType("application/json")
                .body(requestBody)
                .when()
                .post("")
                .then()
                .statusCode(statusCode)
                .and()
                .body("multiplayer", is(expectedResult));

    }

    @Test
    public void verifyUserSupportCheck() {

        ZoneId zoneId = ZoneId.of("Europe/Ljubljana");
        ZonedDateTime ljubljanaTime = ZonedDateTime.ofInstant(Instant.now(), zoneId);

        String expectedResult;

        if (9 <= ljubljanaTime.getHour() && ljubljanaTime.getHour() <= 15
                && ljubljanaTime.getDayOfWeek() != DayOfWeek.SATURDAY
                && ljubljanaTime.getDayOfWeek() != DayOfWeek.SUNDAY) {

            expectedResult = "enabled";
        } else {
            expectedResult = "disabled";
        }

        String requestBody = "{\n" +
                "    \"userId\": \"" + "test_user1" + "\",\n" +
                "    \"timezone\": 1,\n" +
                "    \"countryCode\": \"" + "USA" + "\"\n" +
                "}";
        RestAssured.baseURI  = "http://localhost:8080/v1/fun7/checkServices";

        given().contentType("application/json")
                .body(requestBody)
                .when()
                .post("")
                .then()
                .statusCode(200)
                .and()
                .body("userSupport", is(expectedResult));

    }

    @ParameterizedTest
    @CsvSource({
            "'{\"userId\": \"nonExistingUses\", \"timezone\": 1, \"countryCode\": \"SLO\"}', 404",
            "{}, 400",
            "'{\"param1\": \"nonExistingUses\", \"param2\": 1, \"param3\": \"SLO\"}', 400",
            "'{\"timezone\": 1, \"countryCode\": \"SLO\"}', 400",

    })
    public void exceptionHandlingCheck(String requestBody, int statusCode) {

        RestAssured.baseURI  = "http://localhost:8080/v1/fun7/checkServices";

        given().contentType("application/json")
                .body(requestBody)
                .when()
                .post("")
                .then()
                .statusCode(statusCode);

    }


}
