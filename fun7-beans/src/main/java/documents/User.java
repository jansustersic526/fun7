package documents;

public class User {

    int numFun7Used;

    public User() {

    }

    public User(int numFun7Used) {
        this.numFun7Used = numFun7Used;
    }

    public int getNumFun7Used() {
        return numFun7Used;
    }

    public void setNumFun7Used(int numFun7Used) {
        this.numFun7Used = numFun7Used;
    }

    @Override
    public String toString() {
        return "User{" +
                "numFun7Used=" + numFun7Used +
                '}';
    }
}
