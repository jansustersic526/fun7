package exceptions;

public class Fun7Exception extends Exception {

    int errorCode;

    public Fun7Exception(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
