package beans;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.kumuluz.ee.configuration.utils.ConfigurationUtil;
import okhttp3.*;
import documents.User;
import exceptions.Fun7Exception;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.*;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;
import java.util.concurrent.ExecutionException;

@ApplicationScoped
public class LogicBean {

    private Firestore db;

    private OkHttpClient httpClient;

    private String adsApiBaseUrl;
    private String adsApiUsername;
    private String adsApiPassword;

    @PostConstruct
    public void initialisation() {

        String workingDir = System.getProperty("user.dir");

        if (workingDir.endsWith("\\fun7-api")) {
            // when started from command line
            workingDir = workingDir.substring(0, workingDir.lastIndexOf("\\fun7-api"));
        } else if (workingDir.endsWith("\\fun7-api\\target")) {
            // for integration tests
            workingDir = workingDir.substring(0, workingDir.lastIndexOf("\\fun7-api"));
        }

        workingDir += "\\fun7-beans\\src\\main\\resources\\fun7-280815-4e3c855f8952.json";

        // Firestore initialisation

        InputStream serviceAccount = null;
        GoogleCredentials credentials = null;
        try {
            serviceAccount = new FileInputStream(workingDir);
            credentials = GoogleCredentials.fromStream(serviceAccount);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(credentials)
                .build();
        FirebaseApp.initializeApp(options);

        db = FirestoreClient.getFirestore();

        // Ads API

        adsApiBaseUrl = ConfigurationUtil.getInstance().get("apis.ads.base-url").get();
        adsApiUsername = ConfigurationUtil.getInstance().get("apis.ads.username").get();
        adsApiPassword = ConfigurationUtil.getInstance().get("apis.ads.password").get();

        httpClient = new OkHttpClient.Builder().authenticator(new Authenticator() {
            public Request authenticate(Route route, Response response) throws IOException {
                String credential = Credentials.basic(adsApiUsername, adsApiPassword);
                return response.request().newBuilder().header("Authorization", credential).build();
            }
        }).build();

    }


    /*
     * Multiplayer is a feature that is available only for more skilled players so it should be enabled if
     * user has used “Fun7” game more than 5 times (based on the number of API calls). Also our
     * multiplayer server infrastructure is located in the US so it should be enabled only if the user
     * comes from the US.
     * */
    public boolean checkMultiplayerStatus (String userId, String countryCode) throws Fun7Exception {

        // is request valid
        if (userId == null || userId.isEmpty()) {
            throw new Fun7Exception("Bad request: missing parameter userId.", 400);
        } else if (countryCode == null || countryCode.isEmpty()) {
            throw new Fun7Exception("Bad request: missing parameter countryCode", 400);
        }

        // check firestore for num of fun7 used
        DocumentReference docRef = db.collection("users").document(userId);
        ApiFuture<DocumentSnapshot> future = docRef.get();

        DocumentSnapshot document = null;
        try {
            document = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        User user = null;
        if (document.exists()) {
            // convert document to POJO
            user = document.toObject(User.class);
        }

        if (user == null) {
            throw new Fun7Exception("Bad request: user " + userId + " does not exist.", 404);
        }

        return multiplayerRequirementsCheck(user.getNumFun7Used(), countryCode);

    }

    public boolean multiplayerRequirementsCheck(int numFun7Used, String countryCode) {
        // requirements check
        if (numFun7Used > 5 && countryCode.equals("USA")) {
            return true;
        }

        return false;
    }

    /*
     * Customer support should be enabled only on work days between 9:00 - 15:00 Ljubljana time,
     * because only then support personnel is available.
     * */

    public boolean checkCustomerSupportStatus() {

        ZoneId zoneId = ZoneId.of("Europe/Ljubljana");
        ZonedDateTime ljubljanaTime = ZonedDateTime.ofInstant(Instant.now(), zoneId);

        return customerSupportRequirementsCheck(ljubljanaTime);
    }

    public boolean customerSupportRequirementsCheck(ZonedDateTime ljubljanaTime) {

        if (9 <= ljubljanaTime.getHour()
                && (ljubljanaTime.getHour() <= 14 || (ljubljanaTime.getHour() == 15 && ljubljanaTime.getMinute() == 0))
                && ljubljanaTime.getDayOfWeek() != DayOfWeek.SATURDAY
                && ljubljanaTime.getDayOfWeek() != DayOfWeek.SUNDAY) {

            return true;
        }

        return false;
    }

    /*
     * Ads in the game are served by the external partner so this service should be enabled only if our
     * external partner supports user device. And to know it, we must call partner’s public API which is
     * already provided and is secured by basic access authentication (via HTTP header).
     * */
    public boolean checkAdsStatus(String countryCode) throws Fun7Exception {

        if (countryCode == null || countryCode.isEmpty()) {
            throw new Fun7Exception("Bad request: missing parameter countryCode", 400);
        }

        // call external API

        Request request = new Request.Builder()
                .url(adsApiBaseUrl + "?countryCode=" + countryCode)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {

            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response);

            return adsRequirementsCheck(response.body().string());

        } catch (IOException e) {
            // unsuccessful request, assume ads are disabled
            return false;
        }

    }

    public boolean adsRequirementsCheck(String apiResponseBody) {
        if (apiResponseBody.equals("sure, why not!")) {
            return true;
        } else {
            return false;
        }
    }

}
