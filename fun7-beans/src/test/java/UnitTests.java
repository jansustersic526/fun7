import beans.LogicBean;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UnitTests {

    @ParameterizedTest
    @CsvSource({
            "1, SLO, false",
            "6, SLO, false",
            "1, USA, false",
            "6, USA, true",
            "5, USA, false"
    })
    void multiplayerTest(int numFun7Used, String countryCode, boolean expectedValue) {
        LogicBean logicBean = new LogicBean();
        assertEquals(expectedValue, logicBean.multiplayerRequirementsCheck(numFun7Used, countryCode));
    }

    @ParameterizedTest
    @CsvSource({
            "2020, 6, 21, 9, 0, false",    // sunday, suitable time
            "2020, 6, 21, 19, 0, false",    // sunday, unsuitable time
            "2020, 5, 9, 12, 0, false",   // saturday, suitable time
            "2020, 5, 9, 20, 0, false",    // saturday, unsuitable time
            "2020, 6, 22, 9, 0, true",    // monday, suitable time
            "2020, 6, 22, 0, 0, false",    // monday, unsuitable time
            "2020, 6, 26, 12, 0, true",    // friday, suitable time
            "2020, 6, 26, 21, 0, false",    // friday, unsuitable time
            "2020, 6, 26, 14, 59, true",    // friday, suitable time - EDGE CASE
            "2020, 6, 26, 15, 0, true",    // friday, suitable time - EDGE CASE
            "2020, 6, 26, 15, 1, false",    // friday, unsuitable time - EDGE CASE
            "2020, 6, 26, 8, 59, false",    // friday, suitable time - EDGE CASE
            "2020, 6, 26, 9, 0, true",    // friday, suitable time - EDGE CASE
            "2020, 6, 26, 9, 1, true"   // friday, unsuitable time - EDGE CASE
    })
    void userSupportTest(int year, int month, int dayOfMonth, int hour, int minute, boolean expectedValue) {

        ZoneId zoneId = ZoneId.of("Europe/Ljubljana");
        ZonedDateTime ljubljanaTime = ZonedDateTime.of(year, month, dayOfMonth, hour, minute, 0, 0, zoneId);
        LogicBean logicBean = new LogicBean();

        assertEquals(expectedValue, logicBean.customerSupportRequirementsCheck(ljubljanaTime));
    }

    @ParameterizedTest
    @CsvSource({
            "'sure, why not!', true",
            "you shall not pass!, false",
            "random text, false"
    })
    void adsTest(String adsApiResponse, boolean expectedValue) {
        LogicBean logicBean = new LogicBean();
        assertEquals(expectedValue, logicBean.adsRequirementsCheck(adsApiResponse));
    }

}