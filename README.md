## Assumptions

1. Method other than checkServices takes care of incrementing the number of how many times a user used Fun7. Since check services is probably called at game launch it wouldn't make sense for a user to simply open and close the game 6 times to unlock the multiplayer feature.

2. **ISO 3166-1 alpha 3** is used as country code standard (relevant country codes: USA).

## Requirements

In order to run fun7 service you will need the following:

1. Java 8 (or newer), you can use any implementation:
    * If you have installed Java, you can check the version by typing the following in a command line:
        
        ```
        java -version
        ```

2. Maven 3.2.1 (or newer):
    * If you have installed Maven, you can check the version by typing the following in a command line:
        
        ```
        mvn -version
        ```
        
3. Git:
    * If you have installed Git, you can check the version by typing the following in a command line:
    
        ```
        git --version
        ```
    
## Usage

1. Build fun7 service using maven:

    ```
    mvn clean package
    ```

2. Run fun7 service:

    ```
    java -jar fun7-api/target/fun7-api-1.0.jar
    ```

3. REST API call:

    * checkServices:
    
        The checkServices method can be accessed via POST request on the following URL:
        
        ```
        http://localhost:8080/v1/fun7/checkServices
        ```
        
        Request body must be in type **JSON**. Here is an example of how it shoud look like:
        
        ```
        {
            "userId": "test_user1",  
            "timezone": 5,  
            "countryCode": "USA"  
        }
        ```
    
4. Shut down:

    To shut down the service simply stop the processes in the foreground.

## Testing

1. Unit tests can be run by typing the following in a command line:

    ```
    mvn clean test
    ```
    
2. Integration tests can be run by typing the following in a command line:

    ```
    mvn clean verify
    ```
    
    *Note that this will also run unit tests.*
    
3. For manual testing (e.g. Postman) database currently contains *test_user1* (used Fun7 6 times) and *test_user2* (used Fun7 4 times). Other parameters can be random.